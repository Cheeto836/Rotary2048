#pragma once

#include <memory>

#include <qwidget.h>

class Ui_Controller;

class Controller : public QWidget
{
  Q_OBJECT

private:
  std::shared_ptr<Ui_Controller> m_ui;

public:
  Controller(QWidget* parent = nullptr);

signals:

public slots:
};
