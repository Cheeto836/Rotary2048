#include "DPad.h"

#include "ui_DPad.h"

DPad::DPad(QWidget* parent) : QWidget(parent)
{
  m_ui = std::make_shared<Ui_DPad>();
  m_ui->setupUi(this);

  // setup button texts to unicode arrows
  m_ui->btnDown->setText(QChar(0x2193));
  m_ui->btnUp->setText(QChar(0x2191));
  m_ui->btnLeft->setText(QChar(0x2190));
  m_ui->btnRight->setText(QChar(0x2192));
}
