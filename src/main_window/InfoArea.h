#pragma once

#include <memory>

#include <qwidget.h>

class Ui_InfoArea;

class InfoArea : public QWidget
{
  Q_OBJECT

private:
  std::shared_ptr<Ui_InfoArea> m_ui;

public:
  InfoArea(QWidget* parent = nullptr);

signals:

public slots:
};
