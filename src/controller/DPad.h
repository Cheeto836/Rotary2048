#pragma once

#include <memory>

#include <qwidget.h>

class Ui_DPad;

class DPad : public QWidget
{
  Q_OBJECT

private:
  std::shared_ptr<Ui_DPad> m_ui;

public:
  DPad(QWidget* m_parent = nullptr);

signals:

public slots:
};
