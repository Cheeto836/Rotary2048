#include "Controller.h"

#include "ui_Controller.h"

Controller::Controller(QWidget* parent) : QWidget(parent)
{
  m_ui = std::make_shared<Ui_Controller>();
  m_ui->setupUi(this);

  // set the buttons text to rotation arrows
  m_ui->btnRotateLeft->setText(QChar(0x21A9));
  m_ui->btnRotateRight->setText(QChar(0x21AA));
}
