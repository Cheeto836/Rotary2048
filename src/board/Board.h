#pragma once

#include <memory>

#include <qwidget.h>

class Ui_Board;

class Board : public QWidget
{
  Q_OBJECT

private:
  std::shared_ptr<Ui_Board> m_ui;

public:
  Board(QWidget* parent = nullptr);

signals:

public slots:
};
