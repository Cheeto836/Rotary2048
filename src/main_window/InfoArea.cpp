#include "InfoArea.h"

#include "ui_InfoArea.h"

InfoArea::InfoArea(QWidget* parent) : QWidget(parent)
{
  m_ui = std::make_shared<Ui_InfoArea>();
  m_ui->setupUi(this);
}
