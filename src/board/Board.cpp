#include "Board.h"

#include "ui_Board.h"

Board::Board(QWidget* parent) : QWidget(parent)
{
  m_ui = std::make_shared<Ui_Board>();
  m_ui->setupUi(this);
}
