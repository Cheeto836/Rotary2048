cmake_minimum_required(VERSION 3.5.0)

#Project name
project(Rotary2048)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

#setup qt
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

#fine QtWidgets library
find_package(Qt6 COMPONENTS Widgets REQUIRED)

set(TARGET_ROOT ${CMAKE_BINARY_DIR}/${CMAKE_SYSTEM_NAME})
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${TARGET_ROOT}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${TARGET_ROOT}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${TARGET_ROOT}/bin)

add_subdirectory(board)
add_subdirectory(controller)
add_subdirectory(main_window)

