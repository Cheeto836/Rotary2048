project(controller)

add_library(${PROJECT_NAME} STATIC
  ./DPad.cpp
  ./DPad.h
  ./DPad.ui
  ./Controller.cpp
  ./Controller.h
  ./Controller.ui
)

target_include_directories(${PROJECT_NAME}
  PUBLIC ${CMAKE_CURRENT_LIST_DIR}
)

target_link_libraries(${PROJECT_NAME}
  PRIVATE Qt6::Widgets
)

INSTALL(
  TARGETS ${PROJECT_NAME}
  DESTINATION lib
)
