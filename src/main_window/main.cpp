#include <iostream>

#include <qapplication.h>

#include "MainWindow.h"

int main(int argc, char** argv)
{
  QApplication app(argc, argv);

  MainWindow mainWin;
  mainWin.show();

  return QApplication::exec();
}
