#include "MainWindow.h"

#include "ui_MainWindow.h"

MainWindow::MainWindow()
{
  m_ui = std::make_shared<Ui_MainWindow>();
  m_ui->setupUi(this);
}
