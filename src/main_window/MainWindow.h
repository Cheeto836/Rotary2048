#pragma once

#include <memory>

#include <qmainwindow.h>

class Ui_MainWindow;

class MainWindow : public QMainWindow
{
  Q_OBJECT

private:
  // member variables
  std::shared_ptr<Ui_MainWindow> m_ui;

public:
  MainWindow();

signals:

public slots:
};
