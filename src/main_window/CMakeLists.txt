project(rotarty_2048)

add_executable(${PROJECT_NAME}
  ./main.cpp
  ./InfoArea.cpp
  ./InfoArea.h
  ./InfoArea.ui
  ./MainWindow.cpp
  ./MainWindow.h
  ./MainWindow.ui
)

target_include_directories(${PROJECT_NAME}
  PRIVATE ${CMAKE_CURRENT_LIST_DIR}
)

target_link_libraries(${PROJECT_NAME}
  PRIVATE Qt6::Widgets
  PRIVATE board
  PRIVATE controller
)

INSTALL(
  TARGETS ${PROJECT_NAME}
  DESTINATION lib
)
